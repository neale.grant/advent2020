class ship:
    compass = {'E': 90, 'N': 0, 'S': 180, 'W': 270}
    def __init__(self, orientation):
        if type(orientation) == str:
            self.orientation = ship.compass[orientation]  # Translate to a bearing
        else:
            self.orientation = orientation
        self.x = 0
        self.y = 0

    def turn(self, degrees):
        if degrees % 90 != 0:
            raise ValueError(f"Can only turn at right angles, not {degrees} degrees")
        self.orientation = (self.orientation + degrees) % 360
        if self.orientation < 0:
            self.orientation = self.orientation + 360

    def move_forward(self, distance):
        self.move_directionally(self.orientation, distance)

    def move_directionally(self, direction, distance):
        if type(direction) == str:
            direction = ship.compass[direction]  # Translate to a bearing
        if direction == 0:
            self.y = self.y + distance
        elif direction == 90:
            self.x = self.x + distance
        elif direction == 180:
            self.y = self.y - distance
        elif direction == 270:
            self.x = self.x - distance
        else:
            raise ValueError(f"This ship is listing at unexpected angle {direction} degrees")

boaty_mcboatface = ship('E')

with open('input.txt') as directions:
    for instruction in directions:
        movement = instruction[:1]
        magnitude = int(instruction[1:-1])
        if movement == 'R':
            boaty_mcboatface.turn(magnitude)
        elif movement == 'L':
            boaty_mcboatface.turn(0 - magnitude)
        elif movement == 'F':
            boaty_mcboatface.move_forward(magnitude)
        else:
            boaty_mcboatface.move_directionally(movement, magnitude)

print(f"Boaty is now at coordinates {boaty_mcboatface.x}E, {boaty_mcboatface.y}N")
print("This is {} grid units from the start".format(abs(boaty_mcboatface.x) + abs(boaty_mcboatface.y)))