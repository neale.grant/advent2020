class ship:
    def __init__(self, starting_x_offset, starting_y_offset):
        self.x = 0
        self.y = 0
        self.waypoint_x = starting_x_offset
        self.waypoint_y = starting_y_offset

    def turn_waypoint(self, degrees):
        if degrees % 90 != 0:
            raise ValueError(f"Can only turn at right angles, not {degrees} degrees")
        for i in range(abs(degrees) // 90):
            if degrees > 0:
                self.waypoint_x, self.waypoint_y = self.waypoint_y, 0 - self.waypoint_x
            else:
                self.waypoint_x, self.waypoint_y = 0 - self.waypoint_y, self.waypoint_x

    def move_forward(self, distance):
        self.x = self.x + distance * self.waypoint_x
        self.y = self.y + distance * self.waypoint_y

    def move_waypoint_offset(self, direction, distance):
        if direction == 'N':
            self.waypoint_y = self.waypoint_y + distance
        elif direction == 'E':
            self.waypoint_x = self.waypoint_x + distance
        elif direction == 'S':
            self.waypoint_y = self.waypoint_y - distance
        elif direction == 'W':
            self.waypoint_x = self.waypoint_x - distance
        else:
            raise ValueError(f"Can't move the waypoint in direction {direction}")

boaty_mcboatface = ship(10, 1)

with open('input.txt') as directions:
    for instruction in directions:
        movement = instruction[:1]
        magnitude = int(instruction[1:-1])
        if movement == 'R':
            boaty_mcboatface.turn_waypoint(magnitude)
        elif movement == 'L':
            boaty_mcboatface.turn_waypoint(0 - magnitude)
        elif movement == 'F':
            boaty_mcboatface.move_forward(magnitude)
        else:
            boaty_mcboatface.move_waypoint_offset(movement, magnitude)

print(f"Boaty is now at coordinates {boaty_mcboatface.x}E, {boaty_mcboatface.y}N")
print("This is {} grid units from the start".format(abs(boaty_mcboatface.x) + abs(boaty_mcboatface.y)))