from sys import argv

with open(argv[1]) as timetable:
    timetable.readline()
    bus = [int(b) for b in timetable.readline().translate(str.maketrans('x','0')).split(',')]

interval = bus[0]
timestamp_check = bus[0]

for b in range(1, len(bus)):
    if bus[b] == 0:
        continue
    while (timestamp_check + b) % bus[b] != 0:
        timestamp_check += interval
    interval = interval * bus[b]
    print(f"Found bus[{b}] service {bus[b]} at {timestamp_check}")

# Sanity check because I'm not super-confident about this algorithm...
for b in range(len(bus)):
    if bus[b] != 0:
        print("Service {} leaves at {} (its {} run round the island)".format(
            bus[b], timestamp_check + b, (timestamp_check + b) / bus[b]
        ))

print("Puzzle answer is {}".format(timestamp_check))