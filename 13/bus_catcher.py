earliest_bus = (999999999,0)

with open('input.txt') as timetable:
    target_time = int(timetable.readline()[:-1])
    buses = timetable.readline().split(',')

for bus in buses:
    if bus == 'x':
        continue
    if target_time % int(bus) == 0:
        # There's one leaving now!
        next_departure = 0
    else:
        # It's in its period time less however much we missed it by
        next_departure = int(bus) - target_time % int(bus)
    if next_departure < earliest_bus[0]:
        earliest_bus = (next_departure, int(bus))

print(f"Service {earliest_bus[1]} leaves in {earliest_bus[0]} minutes afer {target_time}")
print("Puzzle answer is {}".format(earliest_bus[0] * earliest_bus[1]))