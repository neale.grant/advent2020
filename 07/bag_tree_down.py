import logging
import re

holdall = {}
rule_format = re.compile("^([\w ]+) bags contain (.*)\.$")
bag_format = re.compile("^(\d)+ ([\w ]+) bag")

class bag:

    def __init__(self, colour):
        self.colour = colour
        self.descendant_count = 0
        self.children = []

    def __repr__(self):
        return self.colour

    def add_child(self, quantity, child):
        self.children.append((quantity, child))

    def count_descendants(self):
        if self.descendant_count == 0:
            for child in self.children:
                self.descendant_count += child[0]
                self.descendant_count += child[0] * child[1].count_descendants()
        return self.descendant_count

with open('input.txt') as bag_rules:
    for rule in bag_rules:
        formatted_rule = rule_format.match(rule)
        if not formatted_rule:
            logging.error(f"Failed to parse {rule} as a rule")
            exit(1)
        parent_bag_colour = formatted_rule.group(1)
        if parent_bag_colour in holdall:
            # Must have already been created as a child in another rule
            parent_bag = holdall[parent_bag_colour]
        else:
            # Create it an add it to the dict
            parent_bag = bag(parent_bag_colour)
            holdall[parent_bag_colour] = parent_bag
        # Trickier now because re doesn't capture multiple entries from a
        # single repeating ()+ group. Maybe there's an alternative regex
        # module that would, but we can do it the hard way.
        child_bag_descriptions = formatted_rule.group(2).split(", ")
        for child_bag_description in child_bag_descriptions:
            if child_bag_description == "no other bags":
                # A special case of no children
                continue
            formatted_bag = bag_format.match(child_bag_description)
            if not formatted_bag:
                logging.error(f"Failed to parse {child_bag_description} as a bag")
                exit(1)
            child_bag_colour = formatted_bag.group(2)
            if child_bag_colour in holdall:
                # Add this new parent to its existing set
                child_bag = holdall[child_bag_colour]
            else:
                # Add it to the dict along with its first parent
                child_bag = bag(child_bag_colour)
                holdall[child_bag_colour] = child_bag
            parent_bag.add_child(int(formatted_bag.group(1)), child_bag)

# Now we should have a dictionary of all the possible colours of bag,
# defined by objects that hold the parent relation. All we need to do
# is choose our bag colour and iterate/recurse over parents until we
# exhaust them all

print("{} bags must be carried in a shiny gold bag"
    .format(holdall['shiny gold'].count_descendants()))
