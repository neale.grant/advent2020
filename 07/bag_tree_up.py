import logging
import re

holdall = {}
rule_format = re.compile("^([\w ]+) bags contain (.*)\.$")
bag_format = re.compile("^\d+ ([\w ]+) bag")

class bag:

    def __init__(self, colour, parent=None):
        self.colour = colour
        self.ancestors = set()
        if parent == None:
            self.parents = []
        else:
            self.parents = [parent]

    def __repr__(self):
        return self.colour

    def add_parent(self, parent):
        if not parent in self.parents:
            self.parents.append(parent)

    def find_ancestors(self):
        if len(self.ancestors) == 0:
            for parent in self.parents:
                self.ancestors.add(parent.colour)
                self.ancestors |= parent.find_ancestors()
        return self.ancestors

with open('input.txt') as bag_rules:
    for rule in bag_rules:
        formatted_rule = rule_format.match(rule)
        if not formatted_rule:
            logging.error(f"Failed to parse {rule} as a rule")
            exit(1)
        parent_bag_colour = formatted_rule.group(1)
        if parent_bag_colour in holdall:
            parent_bag = holdall[parent_bag_colour]
        else:
            # Add it to the dict, but we don't know any of its parents yet
            parent_bag = bag(parent_bag_colour)
            holdall[parent_bag_colour] = parent_bag
        # Trickier now because re doesn't capture multiple entries from a
        # single repeating ()+ group. Maybe there's an alternative regex
        # module that would, but we can do it the hard way.
        child_bag_descriptions = formatted_rule.group(2).split(", ")
        for child_bag_description in child_bag_descriptions:
            if child_bag_description == "no other bags":
                # A special case of no children
                continue
            formatted_bag = bag_format.match(child_bag_description)
            if not formatted_bag:
                logging.error(f"Failed to parse {child_bag_description} as a bag")
                exit(1)
            child_bag_colour = formatted_bag.group(1)
            if child_bag_colour in holdall:
                # Add this new parent to its existing set
                holdall[child_bag_colour].add_parent(parent_bag)
            else:
                # Add it to the dict along with its first parent
                child_bag = bag(child_bag_colour, parent_bag)
                holdall[child_bag_colour] = child_bag

# Now we should have a dictionary of all the possible colours of bag,
# defined by objects that hold the parent relation. All we need to do
# is choose our bag colour and iterate/recurse over parents until we
# exhaust them all

print("{} bags can contain a shiny gold bag"
    .format(len(holdall['shiny gold'].find_ancestors())))

#print("{} bags can contain a shiny gold bag {}"
#    .format(len(holdall['shiny gold'].find_ancestors()), holdall['shiny gold'].find_ancestors()))
