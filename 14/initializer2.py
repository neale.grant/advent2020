from sys import argv

masks = [] # List of two-tuples of actual bitmasks we can or and and
or_mapping = str.maketrans('N','0')
and_mapping = str.maketrans('N','1')

def make_masks(mask):
    # Change 0s to Ns (NOP) so that 0 can be meaningful in mask generation
    original_mask = mask.translate(str.maketrans('0','N'))
    masks.clear()
    template_masks = [original_mask]
    while 'X' in template_masks[0]:
        template_masks.append(template_masks[0].replace('X','0',1))
        template_masks.append(template_masks[0].replace('X','1',1))
        del template_masks[0]
    for template in template_masks:
        masks.append((int(template.translate(or_mapping), base=2), int(template.translate(and_mapping), base=2)))


def apply_masked_update(register, value):
    for mask in masks:
        new_register = (register | mask[0]) & mask[1]
        #print(f"Updating register {new_register} (vs {register}) with {value}")
        memory[new_register] = value


memory = {}

with open(argv[1]) as program:
    for instruction in program:
        if instruction.startswith("mask = "):
            input_mask = instruction[7:-1]
            make_masks(input_mask)
        elif instruction.startswith("mem"):
            register = int(instruction.split(" = ")[0][4:-1])
            value = int(instruction.split(" = ")[1][:-1])
            apply_masked_update(register, value)

print(memory)

print("Puzzle answer = {}".format(sum(memory.values())))