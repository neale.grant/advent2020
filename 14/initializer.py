from sys import argv

memory = {}

with open(argv[1]) as program:
    for instruction in program:
        if instruction.startswith("mask = "):
            fullmask = instruction[7:-1]
            onemask = int(fullmask.translate(str.maketrans('X','0')), base=2)
            zeromask = int(fullmask.translate(str.maketrans('X','1')), base=2)
        elif instruction.startswith("mem"):
            register = int(instruction.split(" = ")[0][4:-1])
            value = int(instruction.split(" = ")[1][:-1])
            memory[register] = (value | onemask) & zeromask
            print(f"Register {register} now contains {memory[register]} (vs {value})")

print(memory)

print("Puzzle answer = {}".format(sum(memory.values())))