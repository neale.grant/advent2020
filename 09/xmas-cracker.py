import logging

"""Check whether a number is the sum of any two of the previous 25 numbers
"""

window_size = 25
number_list = []
logging.basicConfig(level="DEBUG")

def find_two_sum(searchlist, target):
    entry_count = len(searchlist)
    smaller, larger = 0, -1
    direction = -1
    last_difference = 1
    last_move_was_small_increment, found = False, False
    logging.debug(f"  Searching list: {searchlist}")
    while smaller - larger < entry_count:
        this_difference = searchlist[smaller] + searchlist[larger] - target
        logging.debug("{}, {}: {} + {} = {}".format(smaller, larger, searchlist[smaller], + searchlist[larger], + (searchlist[smaller] + searchlist[larger])))
        if this_difference == 0:
            if searchlist[smaller] == searchlist[larger]:
                raise IndexError  # Can't be the same number!
            return (searchlist[smaller], searchlist[larger])
        elif this_difference > 0:   # Collapse the actual value to a +/- indicator
            this_difference = 1
        else:
            this_difference = -1
        if last_move_was_small_increment and (this_difference == 1 or larger < -1): # Don't accidentally scroll beyond the end of the list
            # Need to decide which way to search
            direction = 0 - this_difference
            logging.debug("  Search direction: {}".format(direction))
            last_move_was_small_increment = False
        elif this_difference != last_difference or (this_difference == -1 and larger == -1):
            # Increment the small number
            logging.debug("  Trying next small number")
            smaller += 1
            direction = 0   # Only change one number at a time
            last_move_was_small_increment = True
        larger += direction
        last_difference = this_difference
    raise IndexError

def maintain_window(new_number):
    if len(number_list) == window_size:
        # First check whether the number is valid; if the list isn't full
        # yet, no check is needed or possible
        find_two_sum(sorted(number_list), new_number)
    number_list.append(new_number)
    if len(number_list) > window_size:
        # Don't exceed the maximum size
        del number_list[:1]

with open('input.txt') as source_codes:
    for entry in source_codes:
        logging.debug(f"List is currently: {number_list}")
        try:
            maintain_window(int(entry[:-1]))
        except IndexError:
            print(f'Number {entry[:-1]} is not a sum of any pair of the previous {window_size} numbers')
            exit(1)

print('Unexpectedly, all inputs appear to be valid')