import logging

"""Check whether a number is the sum of any run of consecutive numbers
"""

target = 466456641
number_list = []
logging.basicConfig(level="INFO")

def report_success():
    logging.info(f"Found consecutive numbers equalling the target: {number_list}")
    print("Smallest {} and largest {} members sum to {}".format(
        min(number_list), max(number_list), min(number_list) + max(number_list)
    ))

with open('input.txt') as source_codes:
    for entry in source_codes:
        number_list.append(int(entry[:-1]))
        total = sum(number_list)
        logging.debug(f"List is currently: {number_list}")
        logging.debug(f"Total: {total}")
        while total > target:
            # If it's got too large, shrink it
            del number_list[:1]
            total = sum(number_list)
            logging.debug(f"List is currently: {number_list}")
            logging.debug(f"Total: {total}")
        if total == target:
            report_success()
            exit(0)

logging.error("Got to the end of the file without finding a sequence")