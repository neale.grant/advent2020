# Advent of Code 2020

These are my attempts at solving the [2020 Advent Of Code](https://adventofcode.com/2020) puzzles.

I really don't think there's anything for you to learn here! I'm using the challenge as exercises in teaching myself Python. They should all be working Python, and should all give the right answers, but I won't for a moment claim that they're _good_ Python. Or good code of any sort: I started 20 days late so I sacrificed clean/modular/reusable design, comments, test cases and other Important Stuff in my haste to catch up. I was also inconsistent about creating two separate programs for each of the two daily challenges, and sometimes just mutated the first one into the second.