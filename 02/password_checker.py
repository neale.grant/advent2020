
valid_count = 0

with open('input.txt') as password_file:
    for password_line in password_file:
        parsed_line = password_line.partition(": ")
        password = parsed_line[2]
        format_rules = parsed_line[0].partition(" ")
        required_character = format_rules[2]
        character_bounds = format_rules[0].partition("-")
        character_min = int(character_bounds[0])
        character_max = int(character_bounds[2])
        character_count = password.count(required_character)
        if character_count >= character_min and character_count <= character_max:
            valid_count += 1

print("{} password strings are valid".format(valid_count))