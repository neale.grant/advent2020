
valid_count = 0

with open('input.txt') as password_file:
    for password_line in password_file:
        parsed_line = password_line.partition(": ")
        password = parsed_line[2]
        format_rules = parsed_line[0].partition(" ")
        required_character = format_rules[2]
        character_positions = format_rules[0].partition("-")
        character1 = int(character_positions[0]) - 1
        character2 = int(character_positions[2]) - 1
        if ((password[character1] == required_character) + (password[character2] == required_character)) == 1:
            valid_count += 1

print("{} password strings are valid".format(valid_count))