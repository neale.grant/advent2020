import logging

def run_program(program):
    # Execute program, checking for loops
    instruction_pointer = 0
    visited_lines = set()
    accumulator = 0

    while instruction_pointer < len(program):
        if instruction_pointer in visited_lines:
            logging.warning("Second time visiting instruction {}; accumulator value={}".format(instruction_pointer, accumulator))
            return -1
        visited_lines.add(instruction_pointer)

        if program[instruction_pointer][0] == 'acc':
            accumulator += program[instruction_pointer][1]
            instruction_pointer += 1
        elif program[instruction_pointer][0] == 'nop':
            instruction_pointer += 1
        elif program[instruction_pointer][0] == 'jmp':
            instruction_pointer += program[instruction_pointer][1]
        else:
            logging.error("Unknown instruction {}".format(program[instruction_pointer][0]))
            exit(1)

    return accumulator

original_program = []

# Load program
with open ('input.txt') as listing:
    for line in listing:
        operator = line[:3]
        operand = int(line[5:-1])
        if line[4] == '-':
            operand = 0 - operand
        original_program.append((operator, operand))


# Keep trying to correct the program until it runs...
fixed_instruction = 0
trial_program = original_program[:]

while fixed_instruction < len(trial_program):
    if trial_program[fixed_instruction][0] == 'acc':
        # Can't fix these
        fixed_instruction += 1
        continue
    if trial_program[fixed_instruction][0] == 'nop':
        trial_program[fixed_instruction] = ('jmp', trial_program[fixed_instruction][1])
    else:
        trial_program[fixed_instruction] = ('nop', trial_program[fixed_instruction][1])
    output = run_program(trial_program)
    if output == -1:
        # The new program also hit a loop; revert before the next attempt
        trial_program = original_program[:]
    else:
        print("Final value of accumulator is {}".format(output))
        exit(0)
    fixed_instruction += 1

print("Didn't find a working version of the program")