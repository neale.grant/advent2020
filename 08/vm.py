import logging

program = []

# Load program
with open ('input.txt') as listing:
    for line in listing:
        operator = line[:3]
        operand = int(line[5:-1])
        if line[4] == '-':
            operand = 0 - operand
        program.append((operator, operand))

# Execute program, checking for loops
instruction_pointer = 0
visited_lines = set()
accumulator = 0

while instruction_pointer < len(program):
    if instruction_pointer in visited_lines:
        logging.error("Second time visiting instruction {}; accumulator value={}".format(instruction_pointer, accumulator))
        exit(1)
    visited_lines.add(instruction_pointer)

    if program[instruction_pointer][0] == 'acc':
        accumulator += program[instruction_pointer][1]
        instruction_pointer += 1
    elif program[instruction_pointer][0] == 'nop':
        instruction_pointer += 1
    elif program[instruction_pointer][0] == 'jmp':
        instruction_pointer += program[instruction_pointer][1]
    else:
        logging.error("Unknown instruction {}".format(program[instruction_pointer][0]))
        exit(1)

print("Final value of accumulator is {}".format(accumulator))