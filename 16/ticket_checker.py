from sys import argv

class Rule:
    def __init__(self, rule_text):
        try:
            self.name = rule_text.split(": ")[0]
            self.bounds = []
            for rule_range in rule_text.split(": ")[1].split(" or "):
                    self.bounds.append((int(rule_range.split("-")[0]), int(rule_range.split("-")[1])))
        except:
            print(f"Failed to parse input '{rule_text}'")
            raise

    def is_valid(self, number):
        for rule_range in self.bounds:
            if (number >= rule_range[0] and number <= rule_range[1]):
                return True
        return False

def check_ticket(values):
    for value in [int(i) for i in values.split(",")]:
        for rule in rules:
            if rule.is_valid(value):
                break
        else:
            invalid_values.append(value)

rules = []
invalid_values = []
processing = "Rules"

with open(argv[1]) as input_file:
    for input_line in input_file:
        input_line = input_line.strip()
        if input_line == "":
            continue
        elif input_line == "your ticket:":
            processing = "Your Ticket"
        elif input_line == "nearby tickets:":
            processing = "Other Tickets"
        elif processing == "Rules":
            rules.append(Rule(input_line))
        elif processing == "Your Ticket":
            continue # Ignore for now as instructed
        elif processing == "Other Tickets":
            check_ticket(input_line)
        else:
            print("How did I end up here?")

print("Invalid values are {} with sum {}".format(invalid_values, sum(invalid_values)))
