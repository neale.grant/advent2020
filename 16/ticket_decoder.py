from sys import argv

class Rule:
    def __init__(self, rule_text):
        self.identified = False
        try:
            self.name = rule_text.split(": ")[0]
            self.bounds = []
            for rule_range in rule_text.split(": ")[1].split(" or "):
                    self.bounds.append((int(rule_range.split("-")[0]), int(rule_range.split("-")[1])))
        except:
            print(f"Failed to parse input '{rule_text}'")
            raise

    def is_valid(self, number):
        for rule_range in self.bounds:
            if (number >= rule_range[0] and number <= rule_range[1]):
                return True
        return False

    def link_fields(self, fields):
        self.possible_fields = fields.copy()

    def winnow_fields(self):
        for field in self.possible_fields.copy().keys():
            for value in self.possible_fields[field]:
                if not rule.is_valid(value):
                    #print(f"Field {field} can't be {rule.name}")
                    del self.possible_fields[field]

    def identify_field(self, rules):
        if self.identified or len(self.possible_fields) > 1:
            return
        global unidentified_field_count
        unidentified_field_count -= 1
        self.identified = True
        self.field_number = list(self.possible_fields.keys())[0]
        for r in rules:
            if r != self and self.field_number in r.possible_fields:
                del r.possible_fields[self.field_number]


def check_ticket(values):
    for value in values:
        for rule in rules:
            if rule.is_valid(value):
                break
        else:
            return False
    return True

def store_ticket(value_string):
    values = [int(i) for i in value_string.split(",")]
    if not check_ticket(values):
        return
    if len(fields) == 0:
        # Start collecting them
        for i in range(len(values)):
            fields[i] = [values[i]]
    else:
        for i in range(len(values)):
            fields[i].append(values[i])

fields = {}
rules = []
invalid_values = []
processing = "Rules"

with open(argv[1]) as input_file:
    for input_line in input_file:
        input_line = input_line.strip()
        if input_line == "":
            continue
        elif input_line == "your ticket:":
            processing = "Your Ticket"
        elif input_line == "nearby tickets:":
            processing = "Other Tickets"
        elif processing == "Rules":
            rules.append(Rule(input_line))
        elif processing == "Your Ticket":
            my_ticket = input_line.split(",")
        elif processing == "Other Tickets":
            store_ticket(input_line)
        else:
            print("How did I end up here?")

unidentified_field_count = len(fields)
loops = unidentified_field_count * 2

for rule in rules:
    rule.link_fields(fields)
    rule.winnow_fields()

while unidentified_field_count > 0 and loops > 0:
    for r in rules:
        r.identify_field(rules)
    loops = loops - 1

if unidentified_field_count > 0:
    print("We haven't positively identified all the fields but we've iterated as often as there are fields so we probably won't find any more")
    print("As long as all the departure fields are identified it may not matter if there's ambiguity elsewhere")

puzzle_solution = 1

for r in rules:
    if r.identified:
        print(f"{r.name}: {my_ticket[r.field_number]}")
        if r.name.startswith("departure "):
            puzzle_solution = puzzle_solution * int(my_ticket[r.field_number])
    else:
        print(f"Still think {[k for k in r.possible_fields.keys()]} are possibilities for r.name")
        if r.name.startswith("departure "):
            print(f"That's bad")

print(f"'Departure' field values are {puzzle_solution}")
