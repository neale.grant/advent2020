tree_map = []
tree_multiplier = 1
# x, y, x_hop, y_hop = 0, 0, 3, 1
x_hop, y_hop = [1, 3, 5, 7, 1], [1, 1, 1, 1, 2]

with open('input.txt') as map_file:
    for map_line in map_file:
        tree_map.append(map_line[:-1])  # Strip new lines when reading

for i in range(5):
    x, y, tree_count = 0, 0, 0
    while y < len(tree_map):
        if tree_map[y][x % len(tree_map[y])] == "#":
            tree_count += 1
        x += x_hop[i]
        y += y_hop[i]
    print("Going to hit {} trees on run {}".format(tree_count, i + 1))
    tree_multiplier = tree_multiplier * tree_count

print("Multiply those hits to get {}".format(tree_multiplier))