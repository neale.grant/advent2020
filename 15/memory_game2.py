turns = 30000000

def play_game(numbers):
    history = {}
    for t in range(len(numbers)):
        history[numbers[t]] = t + 1

    print(f"Playing with seed numbers: {history}")

    if numbers[-1] in numbers[:-1]:
        next_number = len(numbers) - numbers.index(numbers[-1]) + 1
    else:
        next_number = 0

    for t in range(len(numbers) + 1, turns + 1):
        # Need to calculate the next value before we store it, because storing
        # it erases the history we need for the calculation
        this_number = next_number
        try:
            next_number = t - history[this_number]
        except KeyError:
            next_number = 0
        history[this_number] = t
        if t % 2000000 == 0:
            print("Turn {}: {}".format(t , this_number))

    print(f"Number {turns} for this game is {this_number}")

play_game([0,3,6])
play_game([1,3,2])
play_game([2,1,3])
play_game([1,2,3])
play_game([2,3,1])
play_game([3,2,1])
play_game([3,1,2])
play_game([9,12,1,4,17,0,18])
