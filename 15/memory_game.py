turns = 2020

def play_game(numbers):
    numbers_reversed = numbers.copy()
    numbers_reversed.reverse()

    print(f"Playing with seed numbers: {numbers}")

    for t in range(len(numbers), turns):
        if numbers[-1] in numbers[0:-1]:
            next_number = numbers_reversed.index(numbers[-1], 1)
        else:
            next_number = 0
        #print("Turn {}: {}".format(t + 1, next_number))
        numbers.append(next_number)
        numbers_reversed.insert(0, next_number)

    print(f"Number {turns} for this game is {numbers[-1]}")

play_game([0,3,6])
play_game([1,3,2])
play_game([2,1,3])
play_game([1,2,3])
play_game([2,3,1])
play_game([3,2,1])
play_game([3,1,2])
play_game([9,12,1,4,17,0,18])
