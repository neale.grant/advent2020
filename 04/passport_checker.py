import re

valid_passport_count = 0
this_passport = []
mandatory_fields = set(["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"])
ecl_list = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
hcl_re = re.compile("^#[0-9a-f]{6}$")
height_metric_re = re.compile("^(\d{3})cm$")
height_imperial_re = re.compile("^(\d{2})in$")
id_re = re.compile("^\d{9}$")
year_re = re.compile("^\d{4}$")

def check_passport(a_passport):
    passport_dict = {}
    for attribute in a_passport:
        passport_dict[attribute.partition(":")[0]] = attribute.partition(":")[2]
    try:
        if (check_eye_colour(passport_dict['ecl'])
            and check_hair_colour(passport_dict['hcl'])
            and check_id(passport_dict['pid'])
            and check_year(passport_dict['byr'], 1920, 2002)
            and check_year(passport_dict['iyr'], 2010, 2020)
            and check_year(passport_dict['eyr'], 2020, 2030)
            and check_height(passport_dict['hgt'])):
            return True
        return False
    except KeyError:
        #print("{} failed validation: missing attribute(s)".format(a_passport))
        return False

def check_eye_colour(a_colour):
    return a_colour in ecl_list
    #if a_colour in ecl_list:
    #    return True
    #print("{} failed eye colour validation".format(a_colour))
    #return False

def check_hair_colour(a_colour):
    return hcl_re.match(a_colour)
    #if hcl_re.match(a_colour):
    #    return True
    #print("{} failed hair colour validation".format(a_colour))
    #return False

def check_height(a_height):
    match = height_metric_re.match(a_height)
    if match:
        height_value = int(match.group(1))
        return height_value >= 150 and height_value <= 193
        #if height_value >= 150 and height_value <= 193:
        #    return True
        #print("{} failed height validation (bounds: 150-193cm)".format(a_height))
        #return False
    match = height_imperial_re.match(a_height)
    if match:
        height_value = int(match.group(1))
        return height_value >= 59 and height_value <= 76
        #if height_value >= 59 and height_value <= 76:
        #    return True
        #print("{} failed height validation (bounds: 150-193cm)".format(a_height))
        #return False
    #print("{} failed height validation (neither metric nor imperial)".format(a_height))
    return False

def check_id(an_id):
    return id_re.match(an_id)
    #if (id_re.match(an_id)):
    #    return True
    #print("{} failed ID validation".format(an_id))
    #return False

def check_year(a_year, earliest, latest):
    return year_re.match(a_year) and int(a_year) >= earliest and int(a_year) <= latest
    #if year_re.match(a_year) and int(a_year) >= earliest and int(a_year) <= latest:
    #    return True
    #print("{} failed year validation (bounds: {}-{})".format(a_year, earliest, latest))
    #return False

with open('input.txt') as passport_file:
    for passport_line in passport_file:
        if passport_line == "\n":
            if (check_passport(this_passport)):
                valid_passport_count += 1
            this_passport[:] = []
        else:
            this_passport.extend(passport_line[:-1].split(" "))

if this_passport and check_passport(this_passport):
    valid_passport_count += 1

print("Found {} valid passports".format(valid_passport_count))