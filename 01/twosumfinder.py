import logging
import time

"""Try to find the three numbers in a list that sum to a target figure
"""

start_time = time.perf_counter()

TARGET = 2020
expenses = []

def find_two_sum(searchlist, target):
    entry_count = len(searchlist)
    smaller, larger = 0, -1
    direction = -1
    last_difference = 1
    last_move_was_small_increment, found = False, False
    while smaller - larger < entry_count:
        this_difference = searchlist[smaller] + searchlist[larger] - target
        #logging.debug("{}, {}: {} + {} = {}".format(smaller, larger, searchlist[smaller], + searchlist[larger], + (searchlist[smaller] + searchlist[larger])))
        if this_difference == 0:
            return (searchlist[smaller], searchlist[larger])
        elif this_difference > 0:   # Collapse the actual value to a +/- indicator
            this_difference = 1
        else:
            this_difference = -1
        if last_move_was_small_increment:
            # Need to decide which way to search
            direction = 0 - this_difference
            #logging.debug("Search direction: {}".format(direction))
            last_move_was_small_increment = False
        elif this_difference != last_difference:
            # Increment the small number
            #logging.debug("Trying next small number")
            smaller += 1
            direction = 0   # Only change one number at a time
            last_move_was_small_increment = True
        larger += direction
        last_difference = this_difference
    raise IndexError


with open('input.txt') as exp_file:
    for entry in exp_file:
        expenses.append(int(entry))

load_time = time.perf_counter()

expenses.sort()

sum_elements = find_two_sum(expenses, TARGET)

end_time = time.perf_counter()

print(f"{sum_elements[0]} + {sum_elements[1]} = {TARGET}")
print(f"Their product is {sum_elements[0] * sum_elements[1]}")
print(f"Found in {end_time - start_time:.6f} seconds ({end_time - load_time :.6f} seconds of processing)")
