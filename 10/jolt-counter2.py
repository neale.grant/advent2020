
class adapter:
    def __init__(self, name):
        self.name = name  # This ended up not being used
        self.next_options = []
        self.saved_count = -1 # Very important to short-cut the recursion for large datasets!
        self.is_last = False
    def add_connector(self, adapter):
        self.next_options.append(adapter)
    def count_connectors(self):
        if self.saved_count == -1:
            if len(self.next_options) == 0:
                if self.is_last:
                    self.saved_count = 1
                else:
                    self.saved_count = 0
            else:
                count = 0
                for next_selection in self.next_options:
                    count = count + next_selection.count_connectors()
                self.saved_count = count
        return self.saved_count


adapters = []
adapter_tree = {0: adapter(0)}

with open('input.txt') as infile:
    for line in infile:
        adapters.append(int(line[:-1]))

adapters.sort()

for entry in adapters:
    this_adapter = adapter(entry)
    adapter_tree[entry] = this_adapter
    # Now link it to any existing ones it's valid for
    for diff in range(1,4):
        if adapter_tree.__contains__(entry - diff):
            adapter_tree[entry - diff].add_connector(this_adapter)

adapter_tree[entry].is_last = True

print("Puzzle answer: {}".format(adapter_tree[0].count_connectors()))
