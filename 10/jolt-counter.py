adapters = []
diffs = [0, 0, 0, 1] # Puzzle rules say there's always a 3-jolt diff at the end

with open('input.txt') as infile:
    for line in infile:
        adapters.append(int(line[:-1]))

adapters.sort()

last_adapter = 0
for adapter in adapters:
    diff = adapter - last_adapter
    diffs[diff] = diffs[diff] + 1
    last_adapter = adapter

print("Differences are:")
print(f"1: {diffs[1]}")
print(f"2: {diffs[2]}")
print(f"3: {diffs[3]}")
print("Puzzle answer: {}".format(diffs[1] * diffs[3]))
