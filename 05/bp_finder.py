lowest_seat_number, highest_seat_number = 1024, 0
occupied_seats = []
mapping = str.maketrans('FBLR','0101')

with open('input.txt') as boarding_passes:
    for boarding_pass in boarding_passes:
        seat = int(boarding_pass[:-1].translate(mapping), 2)
        occupied_seats.append(seat)
        # print(f"{boarding_pass[:-1]} becomes seat number {seat}")
        if seat > highest_seat_number:
            highest_seat_number = seat
        elif seat < lowest_seat_number:
            lowest_seat_number = seat

possible_seats = [i for i in range(lowest_seat_number, highest_seat_number + 1)]

empty_seats = set(possible_seats).difference(set(occupied_seats))

print("Seats {} are missing between {} and {}".format(empty_seats, lowest_seat_number, highest_seat_number))