highest_seat_number = 0
row_mapping = str.maketrans('FB','01')
column_mapping = str.maketrans('LR','01')

with open('input.txt') as boarding_passes:
    for boarding_pass in boarding_passes:
        row = int(boarding_pass[:7].translate(row_mapping), 2)
        column = int(boarding_pass[7:-1].translate(column_mapping), 2)
        seat = 8 * row + column
        print(f"{boarding_pass[:-1]} becomes seat number {seat}")
        if seat > highest_seat_number:
            highest_seat_number = seat

print("The highest seat number found was {}".format(highest_seat_number))