answer_count = 0
group = set("abcdefghijklmnopqrstuvwxyz")

with open('input.txt') as landing_card_file:
    for person in landing_card_file:
        if person == "\n":
            #print(f"All members of this answered {len(group)} questions: {group}")
            answer_count += len(group)
            group = set("abcdefghijklmnopqrstuvwxyz")
        else:
            group = group & set(person[:-1])

if group:
    answer_count += len(group)

print("Found {} questions answered".format(answer_count))