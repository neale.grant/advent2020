answer_count = 0
group = ""

with open('input.txt') as landing_card_file:
    for person in landing_card_file:
        if person == "\n":
            #print(f"Group {group} answered {len(set(group))} questions")
            answer_count += len(set(group))
            group = ""
        else:
            group += person[:-1]

if group:
    answer_count += len(set(group))

print("Found {} questions answered".format(answer_count))