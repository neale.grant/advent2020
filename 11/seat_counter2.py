occupancy = []
previous_occupancy = []


def occupied_directional_neighbour(seats, seat_x, seat_y, x_dir, y_dir):
    while True:
        seat_x = seat_x + x_dir
        seat_y = seat_y + y_dir
        try:
            if seat_y < 0 or seat_y >= len(seats) or seat_x < 0 or seat_x >= len(seats[seat_y]) or seats[seat_y][seat_x] == 'L':
                return 0
            if seats[seat_y][seat_x] == '#':
                return 1
        except IndexError:
            print(f"Shouldn't have tried {seat_x},{seat_y}")
            raise

def occupied_neighbours(seats, seat_x, seat_y):
    occupied_count = occupied_directional_neighbour(seats, seat_x, seat_y, -1, -1) # up-left
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, 0, -1) # up
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, 1, -1) # up-right
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, 1, 0) # right
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, 1, 1) # down-right
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, 0, 1) # down
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, -1, 1) # down-left
    occupied_count = occupied_count + occupied_directional_neighbour(seats, seat_x, seat_y, -1, 0) # left
    return occupied_count
    

with open('input.txt') as infile:
    for row in infile:
        occupancy.append(row[:-1])

while occupancy != previous_occupancy:
    # Iterate until it stabilizes
    previous_occupancy = occupancy.copy()
    for row in range(len(occupancy)):
        for column in range(len(occupancy[row])):
            seat = occupancy[row][column]
            if seat == '.':
                continue
            neighbour_count = occupied_neighbours(previous_occupancy, column, row)
            if seat == 'L' and neighbour_count == 0:
                seat = '#'
            elif seat == '#' and neighbour_count >= 5:
                seat = 'L'
            occupancy[row] = occupancy[row][:column] + seat + occupancy[row][column + 1:]

print("Current occupancy:")
for row_state in occupancy:
    print(row_state)

occupied_count = 0
for row_state in occupancy:
    occupied_count = occupied_count + row_state.count('#')

print(f"A total of {occupied_count} seats are occupied")