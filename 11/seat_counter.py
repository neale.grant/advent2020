occupancy = []
previous_occupancy = []


def occupied_neighbours(seats, seat_x, seat_y):
    min_x = max(seat_x - 1, 0)
    max_x = min(seat_x + 2, len(seats[seat_y]))
    min_y = max(seat_y - 1, 0)
    max_y = min(seat_y + 2, len(seats))
    occupied_count = 0
#    print(f"{seat_x},{seat_y}", end=':')
    for y in range(min_y, max_y):
        for x in range(min_x, max_x):
#            print(f"{x},{y}={seats[y][x]}", end=' ')
            if (x != seat_x or y != seat_y) and seats[y][x] == "#":
                occupied_count = occupied_count + 1
    return occupied_count
    

with open('input.txt') as infile:
    for row in infile:
        occupancy.append(row[:-1])

while occupancy != previous_occupancy:
    # Iterate until it stabilizes
    previous_occupancy = occupancy.copy()
    for row in range(len(occupancy)):
        for column in range(len(occupancy[row])):
            seat = occupancy[row][column]
            if seat == '.':
                continue
            neighbour_count = occupied_neighbours(previous_occupancy, column, row)
#            print(f"Counted {neighbour_count} neighbours for seat at {row},{column}")
            if seat == 'L' and neighbour_count == 0:
#                print(f"  L -> #")
                seat = '#'
            elif seat == '#' and neighbour_count >= 4:
#                print(f"  # -> L")
                seat = 'L'
            occupancy[row] = occupancy[row][:column] + seat + occupancy[row][column + 1:]
#    print("Current occupancy:")
#    for row_state in occupancy:
#        print(row_state)
#    print("Previous occupancy:")
#    for row_state in previous_occupancy:
#        print(row_state)


occupied_count = 0
for row_state in occupancy:
    occupied_count = occupied_count + row_state.count('#')

print(f"A total of {occupied_count} seats are occupied")